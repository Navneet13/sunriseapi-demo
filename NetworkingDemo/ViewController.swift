//
//  ViewController.swift
//  NetworkingDemo
//
//  Created by Navpreet Kaur on 2019-10-16.
//  Copyright © 2019 Navneet. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet weak var resultsLabel: UILabel!
    
    //MARK: Built-In Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    //MARK: Custom Functions

    @IBAction func checkOutButton(_ sender: Any) {
        
//        1. Go to the website you want to get data from
        let websiteURL = "https://api.sunrise-sunset.org/json?lat=36.7201600&lng=-4.4203400&date=2019-03-04"
        
        AF.request(websiteURL).responseJSON { response in
            
            //2. Code for dealing with JSON Response
            print(response)
            
            //Convert the response to a JSON Object
            let jsonResponse = JSON(response.value)
            let sunrise = jsonResponse["results"]["sunrise"]
            let sunset = jsonResponse["results"]["sunset"]
            
            print("\(sunrise),\(sunset)")
            
            self.resultsLabel.text = "Sunrise: \(sunrise)"
            
            
        }
        
        
     }
        
}

